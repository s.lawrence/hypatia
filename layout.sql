CREATE TABLE sources (
	source_id INTEGER PRIMARY KEY,
	source_name TEXT,
	updated DATETIME DEFAULT '1900-01-01'
);

-- TODO not the right way to do this. For starters, not all IDs are linked to a source.
CREATE TABLE foreign_ids (
	id_type INTEGER,
	source_id INTEGER,
	id INTEGER,
	foreign_id TEXT
);

INSERT INTO sources (source_name) VALUES ("arXiv");
INSERT INTO sources (source_name) VALUES ("InspireHEP");



CREATE TABLE papers (
	paper_id INTEGER PRIMARY KEY,
	title TEXT,
	first_published DATETIME,
	published DATETIME,
	updated DATETIME
);

CREATE TABLE authors (
	author_id INTEGER PRIMARY KEY,
	keyname TEXT,
	fullname TEXT,
	updated DATETIME
);

CREATE TABLE institutions (
	institution_id INTEGER PRIMARY KEY,
	updated DATETIME
);


CREATE TABLE affiliations (
	author_id INTEGER NOT NULL,
	institution_id INTEGER NOT NULL,
	joined DATETIME,
	departed DATETIME
);
CREATE INDEX affiliations_author_idx ON affiliations (author_id);
CREATE INDEX affiliations_institution_idx ON affiliations (institution_id);

CREATE TABLE authorship (
	paper_id INTEGER NOT NULL,
	author_id INTEGER NOT NULL
);
CREATE INDEX authorship_paper_idx ON authorship (paper_id);
CREATE INDEX authorship_author_idx ON authorship (author_id);


CREATE TABLE paper_source (
	paper_id INTEGER,
	filetype TEXT,
	url TEXT
);
CREATE INDEX paper_source_idx ON paper_source (paper_id);

CREATE TABLE citations (
	paper_id INTEGER,
	citation INTEGER
);
CREATE INDEX citations_paper_idx ON citations (paper_id);
CREATE INDEX citations_citation_idx ON citations (citation);

-- TODO keywords

