#!/usr/bin/env python

"""
List, in reverse chronological order, all citations to an author.
"""

import json

import urllib.request
from urllib.parse import quote

#userid = 'H.Lamm.1'
userid = 'Scott.Lawrence.1'

query = 'exactauthor:{}'.format(userid)

with urllib.request.urlopen("http://inspirehep.net/search?p={}&of=recjson&rg=250&ot=title,number_of_citations,recid".format(quote(query))) as response:
    papers = json.loads(response.read())

ncite = 0
for p in papers:
    ncite += p['number_of_citations']

print('{} papers found'.format(len(papers)))
print('{} citations reported'.format(ncite))

query = ' or '.join('refersto:recid:{}'.format(p['recid']) for p in papers)

import subprocess
subprocess.call(['xdg-open', 'http://inspirehep.net/search?p={}'.format(quote(query))])
