#!/usr/bin/env python

import argparse
import logging
import sqlite3

import hypatia.harvest.arxiv as arxiv
import hypatia.harvest.inspire as inspire

logging.basicConfig(format='{asctime} [{levelname}] {name}: {message}', style='{', level=logging.DEBUG)

logger = logging.getLogger('hypatia.update')
logger.info('Hypatia update beginning')

parser = argparse.ArgumentParser(description='Hypatia updater')
args = parser.parse_args()

if False:
    harvester = inspire.InspireHarvester(sqlite3.connect('librarydb'))
    harvester.run()
    #it = harvester.sickle.ListRecords(metadataPrefix='oai_dc')
    #print(it)

else:
    harvester = arxiv.ArxivHarvester(sqlite3.connect('library.db'))
    harvester.run()

    if False:
        print(harvester.updated)
        #print('{}-{:02}-{:02}'.format(lastupdate.year, lastupdate.month, lastupdate.day))

        req = {'metadataPrefix': 'arXiv'}
        req['from'] = '1990-01-01'
        req['until'] = '2003-01-01'
        recs = harvester.sickle.ListRecords(**req)
        for rec in recs:
            if False:
                print(rec.metadata)

lib = sqlite3.connect('library.db')
lib.execute('VACUUM')
lib.close()

