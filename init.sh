#!/bin/sh

if [ -f library.db ]; then
	echo "Backing up old library..."
	mv library.db library-old.db
fi

echo "Initializing library..."
exec sqlite3 library.db -batch < layout.sql
