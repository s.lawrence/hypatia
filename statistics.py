#!/usr/bin/env python

import sqlite3

db = sqlite3.connect('library.db')

def count(table):
    return db.execute('SELECT COUNT(*) FROM {};'.format(table)).fetchall()[0][0]

counts = [
        'sources',
        'papers'
    ]

for s in counts:
    print('{} {}'.format(count(s), s))
