import datetime 
import sickle
import threading

class Harvester(threading.Thread):
    def __init__(self, lib, sourcename):
        self.lib = lib
        self.__sourcename = sourcename
        self.updated = self.__getUpdated()

    def __getUpdated(self):
        query = 'SELECT date(updated) FROM sources WHERE source_name=:sourcename'
        a = self.lib.execute(query, {'sourcename': self.__sourcename}).fetchone()[0]
        updated = datetime.date.fromisoformat(a)
        return updated

    def __setUpdated(self, date=None):
        if date:
            query = 'UPDATE sources SET updated=:date WHERE source_name=:sourcename'
            self.lib.execute(query, {'sourcename': self.__sourcename, 'date': date})
            self.lib.commit()
        else:
            query = 'UPDATE sources SET updated=date() WHERE source_name=:sourcename'
            self.lib.execute(query, {'sourcename': self.__sourcename})
            self.lib.commit()

    def __next_date(self, date):
        if date.year < 1980:
            td = datetime.timedelta(days=3650)
        elif date.year < 2000:
            td = datetime.timedelta(days=365)
        elif date.year < 2010:
            td = datetime.timedelta(days=30)
        else:
            td = datetime.timedelta(days=1)
        return date + td

    def __del__(self):
        self.lib.close()

    def run(self):
        req = {'metadataPrefix': self.metadataPrefix}
        date = self.updated + datetime.timedelta(days=1)
        while date <= datetime.date.today():
            req['from'] = date
            date = self.__next_date(date)
            req['until'] = date
            try:
                recs = self.sickle.ListRecords(**req)
                for rec in recs:
                    self.store(rec.metadata)
                self.lib.commit()
            except sickle.oaiexceptions.NoRecordsMatch:
                pass
            self.__setUpdated(date)
            date += datetime.timedelta(days=1)

    def store(self, metadata):
        print(metadata['id'])
        query = 'INSERT INTO papers (title, first_published, published, updated) VALUES (:title, :first_published, :published, :updated)'
        dat = {
                'title': metadata['title'],
                'first_published': metadata['created'],
                'published': metadata['updated'],
                'updated': datetime.datetime.now(),
        }
        self.lib.execute(query, dat)
