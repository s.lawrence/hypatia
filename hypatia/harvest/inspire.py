import sickle

from .harvester import Harvester

class InspireRecord(sickle.models.Record):
    pass

class InspireHarvester(Harvester):
    def __init__(self, lib):
        super(InspireHarvester, self).__init__(lib, 'InspireHEP')
        self.sickle = sickle.Sickle('http://inspirehep.net/oai2d')
        self.sickle.class_mapping['ListRecords'] = InspireRecord
        self.sickle.class_mapping['GetRecord'] = InspireRecord
        self.metadataPrefix = 'oai_dc'
