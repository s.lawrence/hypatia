import sickle

from .harvester import Harvester

class ArxivRecord(sickle.models.OAIItem):
    """Represents an arXiv record.

    :param record_element: The XML element 'record'.
    :type record_element: :class:`lxml.etree._Element`
    :param strip_ns: Flag for whether to remove the namespaces from the
                     element names.
    """

    def __init__(self, record_element, strip_ns=True):
        super(ArxivRecord, self).__init__(record_element, strip_ns=strip_ns)
        oains = self._oai_namespace
        arns = '{http://arxiv.org/OAI/arXiv/}'
        self.header = sickle.models.Header(self.xml.find(
            './/' + self._oai_namespace + 'header'))
        self.deleted = self.header.deleted
        if not self.deleted:
            self.metadata = {}
            mdxml = self.xml.find(oains + 'metadata/' + arns + 'arXiv')
            simple = ['id', 'created', 'updated', 'title', 'categories', 'comments',
                    'report-no', 'journal-ref', 'doi', 'abstract']
            for n in simple:
                e = mdxml.find(arns+n)
                if e is not None:
                    self.metadata[n] = e.text
                else:
                    self.metadata[n] = None

            authors = []
            for axml in mdxml.findall(arns+'authors/'+arns+'author'):
                knxml = axml.find(arns+'keyname')
                fnxml = axml.find(arns+'forenames')
                keyname = axml.find(arns+'keyname').text if knxml else None
                forenames = axml.find(arns+'forenames').text if fnxml else None
                if keyname and forenames:
                    fullname = forenames + ' ' + keyname
                elif keyname:
                    fullname = keyname
                else:
                    fullname = forenames
                authors.append({
                    'keyname': keyname,
                    'fullname': fullname
                })
            self.metadata['authors'] = authors

    def __repr__(self):
        if self.header.deleted:
            return '<ArxivRecord %s [deleted]>' % self.header.identifier
        else:
            return '<ArxivRecord %s>' % self.header.identifier

    def __iter__(self):
        return iter(self.metadata.items())

class ArxivHarvester(Harvester):
    def __init__(self, lib):
        super(ArxivHarvester, self).__init__(lib, 'arXiv')
        self.sickle = sickle.Sickle('http://export.arxiv.org/oai2')
        self.sickle.class_mapping['ListRecords'] = ArxivRecord
        self.sickle.class_mapping['GetRecord'] = ArxivRecord
        self.metadataPrefix = 'arXiv'

